package com.arvs.ebikes.chococenter.expendable_recycler_view;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.arvs.ebikes.chococenter.R;
import com.arvs.ebikes.chococenter.chocolate_category.ChocolateExpandAdapter;

import java.util.ArrayList;

public class MyActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ArrayList<MobileOS> mobileOSes;
    private ChocolateExpandAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my);

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        mobileOSes = new ArrayList<>();

        setData();
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new ChocolateExpandAdapter(this, mobileOSes);
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        adapter.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        adapter.onRestoreInstanceState(savedInstanceState);
    }

    private void setData() {
        ArrayList<Phone> iphones = new ArrayList<>();
        iphones.add(new Phone(R.mipmap.a));
        iphones.add(new Phone(R.mipmap.a));
        iphones.add(new Phone(R.mipmap.a));



        ArrayList<Phone> nexus = new ArrayList<>();
        nexus.add(new Phone(R.mipmap.c));
        nexus.add(new Phone(R.mipmap.c));
        nexus.add(new Phone(R.mipmap.c));


        ArrayList<Phone> windowPhones = new ArrayList<>();
        windowPhones.add(new Phone(R.mipmap.b));
        windowPhones.add(new Phone(R.mipmap.b));
        windowPhones.add(new Phone(R.mipmap.b));



        mobileOSes.add(new MobileOS("iOS", iphones));
        mobileOSes.add(new MobileOS("Android", nexus));
        mobileOSes.add(new MobileOS("Window Phone", windowPhones));
    }
}