package com.arvs.ebikes.chococenter.chocolate_category;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arvs.ebikes.chococenter.R;
import com.arvs.ebikes.chococenter.expendable_recycler_view.MobileOS;
import com.arvs.ebikes.chococenter.expendable_recycler_view.Phone;
import com.arvs.ebikes.chococenter.expendable_recycler_view.RecyclerAdapter;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class ChocolateFragment extends Fragment {

    private RecyclerView recyclerView;
    private ArrayList<MobileOS> mobileOSes = new ArrayList<MobileOS>();
    private Context context;
    private RecyclerAdapter adapter;


    public ChocolateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View view=inflater.inflate(R.layout.fragment_chocolate, container, false);

        recyclerView=view.findViewById(R.id.myrecycler_view);

        setData();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new RecyclerAdapter(getActivity(), mobileOSes);
        recyclerView.setAdapter(adapter);
//        ChocolateAdapter adapter = new ChocolateAdapter(clockHomeList(),context);
//        recyler.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false));
//        recyler.setAdapter(adapter);

        return view;

    }
    private void setData() {

        ArrayList<Phone> iphones = new ArrayList<>();
        iphones.add(new Phone(R.mipmap.chocolate1));
        iphones.add(new Phone(R.mipmap.chocolate2));
        iphones.add(new Phone(R.mipmap.chocolate3));
        iphones.add(new Phone(R.mipmap.chocolate4));




        ArrayList<Phone> nexus = new ArrayList<>();
        nexus.add(new Phone(R.mipmap.chocolate_accessories1));
        nexus.add(new Phone(R.mipmap.chocolate_accessories2));
        nexus.add(new Phone(R.mipmap.chocolate_accessories3));
        nexus.add(new Phone(R.mipmap.chocolate_accessories4));



        ArrayList<Phone> windowPhones = new ArrayList<>();
        windowPhones.add(new Phone(R.mipmap.chocolate_boxes1));
        windowPhones.add(new Phone(R.mipmap.chocolate_boxes2));
        windowPhones.add(new Phone(R.mipmap.chocolate_boxes3));
        windowPhones.add(new Phone(R.mipmap.chocolate_boxes4));


        ArrayList<Phone> windowPhones1 = new ArrayList<>();
        windowPhones1.add(new Phone(R.mipmap.chocolate_equiments1));
        windowPhones1.add(new Phone(R.mipmap.chocolate_equiments2));
        windowPhones1.add(new Phone(R.mipmap.chocolate_equiments3));
        windowPhones1.add(new Phone(R.mipmap.chocolate_equiments4));


        ArrayList<Phone> windowPhones2 = new ArrayList<>();
        windowPhones2.add(new Phone(R.mipmap.chocolate_molds1));
        windowPhones2.add(new Phone(R.mipmap.chocolate_molds2));
        windowPhones2.add(new Phone(R.mipmap.chocolate_molds3));
        windowPhones2.add(new Phone(R.mipmap.chocolate_molds4));


        ArrayList<Phone> windowPhones3 = new ArrayList<>();
        windowPhones3.add(new Phone(R.mipmap.chocolate_paper1));
        windowPhones3.add(new Phone(R.mipmap.chocolate_paper2));
        windowPhones3.add(new Phone(R.mipmap.chocolate_paper3));
        windowPhones3.add(new Phone(R.mipmap.chocolate_paper4));






        mobileOSes.add(new MobileOS("iOS", iphones));
        mobileOSes.add(new MobileOS("Android", nexus));
        mobileOSes.add(new MobileOS("Window Phone", windowPhones));
        mobileOSes.add(new MobileOS("Window Phone1", windowPhones1));
        mobileOSes.add(new MobileOS("Window Phone2", windowPhones2));
        mobileOSes.add(new MobileOS("Window Phone3", windowPhones3));
    }
}
