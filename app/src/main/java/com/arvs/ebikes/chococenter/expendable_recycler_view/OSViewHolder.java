package com.arvs.ebikes.chococenter.expendable_recycler_view;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.arvs.ebikes.chococenter.R;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.GroupViewHolder;

public class OSViewHolder extends GroupViewHolder {

    private ImageView osName;

    public OSViewHolder(View itemView) {
        super(itemView);

        osName = (ImageView) itemView.findViewById(R.id.mobile_os);

    }

    @Override
    public void expand() {
//        osName.setImageResource(R.mipmap.h);
        Log.i("Adapter", "expand");

    }

    @Override
    public void collapse() {
        Log.i("Adapter", "collapse");
//        osName.setImageResource(R.mipmap.i);
    }

    public void setGroupName(ExpandableGroup group) {
//        osName.setText(group.getTitle());
    }
}