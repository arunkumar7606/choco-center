package com.arvs.ebikes.chococenter.chocolate_category;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.arvs.ebikes.chococenter.R;

import java.util.ArrayList;

public class ChocolateAdapter extends RecyclerView.Adapter<ChocolateAdapter.MyViewHolder> {

    private ArrayList<Chocalate_item_Models> arrayList;
    private Context context;
    private LayoutInflater layoutInflater;
    private View view;

    public ChocolateAdapter() {
    }

    public ChocolateAdapter(ArrayList<Chocalate_item_Models> arrayList, Context context) {
        this.arrayList = arrayList;
        this.context = context;
        this.layoutInflater = LayoutInflater.from(context);

    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = layoutInflater.inflate(R.layout.chocolate_view, null, false);
        ChocolateAdapter.MyViewHolder myViewHolder = new ChocolateAdapter.MyViewHolder(view);

        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.imageView_chocolate.setImageResource(arrayList.get(position).getImage_choco_second());
        holder.imageView_chocolate_offer.setImageResource(arrayList.get(position).getImage_choco_offer());

    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public ImageView imageView_chocolate, imageView_chocolate_offer;

        public MyViewHolder(View itemView) {
            super(itemView);

            imageView_chocolate = itemView.findViewById(R.id.imageView_chocolate);
            imageView_chocolate_offer = itemView.findViewById(R.id.imageView_chocolate_offer);
        }
    }
}
