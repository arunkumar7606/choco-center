package com.arvs.ebikes.chococenter.expendable_recycler_view;

import android.os.Parcel;
import android.os.Parcelable;

public class Phone implements Parcelable {

    private int name;

    public Phone() {
    }

    public Phone(int name) {
        this.name = name;
    }

    protected Phone(Parcel in) {
        name = in.readInt();
    }

    public static final Creator<Phone> CREATOR = new Creator<Phone>() {
        @Override
        public Phone createFromParcel(Parcel in) {
            return new Phone(in);
        }

        @Override
        public Phone[] newArray(int size) {
            return new Phone[size];
        }
    };

    public int getName() {
        return name;
    }

    public void setName(int name) {
        this.name = name;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(name);
    }
}