package com.arvs.ebikes.chococenter.bottom_nav_main;

import android.content.Intent;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.arvs.ebikes.chococenter.R;
import com.arvs.ebikes.chococenter.cake_category.CakeFragment;
import com.arvs.ebikes.chococenter.chocolate_category.ChocolateFragment;
import com.arvs.ebikes.chococenter.profile.ProfileActivity;
import com.arvs.ebikes.chococenter.soap_category.SoapFragment;
import com.arvs.ebikes.chococenter.view_pager.ViewPagerAdapter;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import me.relex.circleindicator.CircleIndicator;


public class BottomNavMainActivity extends AppCompatActivity {

    private TextView navheader_textview;
    private Toolbar toolbar;
    private Bundle bundle;
    private Spinner spinner;
    private Fragment fragment;
    private ImageView imageView_profilew;
    private static ViewPager mpager;
    private static int currentPage=0;
    private static final Integer[] pic={R.mipmap.banner3, R.mipmap.banner2, R.mipmap.banner1 };


    private BottomNavigationView.OnNavigationItemSelectedListener monNavigationItemReselectedListener = new BottomNavigationView.OnNavigationItemSelectedListener() {
        @Override
        public boolean onNavigationItemSelected(MenuItem item) {

            switch (item.getItemId()) {
                case R.id.nav_Chocolates:
//                    getSupportActionBar().setTitle("Chocolates");
                    fragment = new ChocolateFragment();
                    loadFragment(fragment);
//                    Toast.makeText(BottomNavMainActivity.this, "Chocolates", Toast.LENGTH_SHORT).show();
                    return true;

                case R.id.nav_cake:
//                    getSupportActionBar().setTitle("Cake");
                    fragment = new CakeFragment();
                    loadFragment(fragment);
//                    Toast.makeText(BottomNavMainActivity.this, "Cake", Toast.LENGTH_SHORT).show();
                    return true;

                case R.id.nav_soap:
//                    getSupportActionBar().setTitle("Soap");
                    fragment = new SoapFragment();
                    loadFragment(fragment);
//                    Toast.makeText(BottomNavMainActivity.this, "soap", Toast.LENGTH_SHORT).show();
                    return true;
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bottom_nav_main);

        pagerSlide();
        imageView_profilew=findViewById(R.id.imageView_profilew);

        imageView_profilew.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(BottomNavMainActivity.this, ProfileActivity.class));
            }
        });


        fragment = new ChocolateFragment();
        loadFragment(fragment);
//        Toast.makeText(BottomNavMainActivity.this, "Chocolates", Toast.LENGTH_SHORT).show();

        spinner = findViewById(R.id.spinner_nav);

        toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
//        getSupportActionBar().setIcon(R.drawable.profilew);
//

        BottomNavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setOnNavigationItemSelectedListener(monNavigationItemReselectedListener);
        getSupportActionBar().setTitle(null);

        String value[] = getResources().getStringArray(R.array.Cities_name);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, value);
        spinner.setAdapter(adapter);
//        spinner.setBackgroundColor(getResources().getColor(R.color.white));


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {

            case R.id.about_Us:
                Toast.makeText(this, "about_Us", Toast.LENGTH_SHORT).show();
                break;

            case R.id.courier_Servicess:
                Toast.makeText(this, "courier_Servicess", Toast.LENGTH_SHORT).show();
                break;

            case R.id.terms_Conditions:
                Toast.makeText(this, "terms_Conditions", Toast.LENGTH_SHORT).show();
                break;

            case R.id.contact_Us:
                Toast.makeText(this, "contact_Us", Toast.LENGTH_SHORT).show();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.disallowAddToBackStack();
        transaction.commit();
    }
    public void pagerSlide(){
        ArrayList<Integer> picArray=new ArrayList<Integer>();


        for (int i=0;i<pic.length;i++)
            picArray.add(pic[i]);

        mpager= findViewById(R.id.pager);
        mpager.setAdapter(new ViewPagerAdapter(picArray,this));

        CircleIndicator indicator=findViewById(R.id.indicator);
        indicator.setViewPager(mpager);

        final Handler handler=new Handler();

        final  Runnable update=new Runnable() {
            @Override
            public void run() {
                if (currentPage==pic.length){
                    currentPage=0;
                }
                mpager.setCurrentItem(currentPage++,true);
            }
        };

        Timer swipeTimer= new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {

                handler.post(update);
            }
        },3000,3000);
    }

}
