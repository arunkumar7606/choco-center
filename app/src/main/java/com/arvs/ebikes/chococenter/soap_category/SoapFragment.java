package com.arvs.ebikes.chococenter.soap_category;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arvs.ebikes.chococenter.R;
import com.arvs.ebikes.chococenter.cake_category.CakeExpandAdapter;
import com.arvs.ebikes.chococenter.chocolate_category.Chocalate_item_Models;
import com.arvs.ebikes.chococenter.chocolate_category.ChocolateAdapter;
import com.arvs.ebikes.chococenter.expendable_recycler_view.MobileOS;
import com.arvs.ebikes.chococenter.expendable_recycler_view.Phone;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class SoapFragment extends Fragment {


    private RecyclerView recyclerView;
    private ArrayList<MobileOS> mobileOSes = new ArrayList<MobileOS>();
    private Context context;
    private SoapExpandAdapter adapter;

    public SoapFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.context = context;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view= inflater.inflate(R.layout.fragment_soap, container, false);
        recyclerView=view.findViewById(R.id.myrecycler_view1);


        setData();
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);

        adapter = new SoapExpandAdapter(getActivity(), mobileOSes);
        recyclerView.setAdapter(adapter);


        return view;

    }


    private void setData() {
        ArrayList<Phone> iphones = new ArrayList<>();
        iphones.add(new Phone(R.mipmap.a));
        iphones.add(new Phone(R.mipmap.a));
        iphones.add(new Phone(R.mipmap.a));



        ArrayList<Phone> nexus = new ArrayList<>();
        nexus.add(new Phone(R.mipmap.c));
        nexus.add(new Phone(R.mipmap.c));
        nexus.add(new Phone(R.mipmap.c));


        ArrayList<Phone> windowPhones = new ArrayList<>();
        windowPhones.add(new Phone(R.mipmap.b));
        windowPhones.add(new Phone(R.mipmap.b));
        windowPhones.add(new Phone(R.mipmap.b));



        mobileOSes.add(new MobileOS("iOS", iphones));
        mobileOSes.add(new MobileOS("Android", nexus));
        mobileOSes.add(new MobileOS("Window Phone", windowPhones));
    }

}
