package com.arvs.ebikes.chococenter.expendable_recycler_view;

import android.view.View;
import android.widget.ImageView;

import com.arvs.ebikes.chococenter.R;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;
import com.thoughtbot.expandablerecyclerview.viewholders.ChildViewHolder;

public class PhoneViewHolder extends ChildViewHolder {

    private ImageView phoneName;

    public PhoneViewHolder(View itemView) {
        super(itemView);

        phoneName = itemView.findViewById(R.id.phone_name);
    }

    public void onBind(Phone phone, ExpandableGroup group) {
        phoneName.setImageResource(phone.getName());
//        if (group.getTitle().equals("Android")) {
//            phoneName.setImageResource(R.mipmap.a);
//        } else if (group.getTitle().equals("iOS")) {
//            phoneName.setImageResource(R.mipmap.b);
//        } else {
//            phoneName.setImageResource(R.mipmap.c);
//        }
    }
}

