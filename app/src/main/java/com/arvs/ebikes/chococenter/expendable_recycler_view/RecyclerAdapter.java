package com.arvs.ebikes.chococenter.expendable_recycler_view;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.arvs.ebikes.chococenter.R;
import com.thoughtbot.expandablerecyclerview.ExpandableRecyclerViewAdapter;
import com.thoughtbot.expandablerecyclerview.models.ExpandableGroup;

import java.util.List;

public class RecyclerAdapter extends ExpandableRecyclerViewAdapter<OSViewHolder, PhoneViewHolder> {

    private Activity activity;
    private View view ;

    public RecyclerAdapter(Activity activity, List<? extends ExpandableGroup> groups) {
            super(groups);
            this.activity = activity;
        }

        @Override
        public OSViewHolder onCreateGroupViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.group_view_holder, parent, false);


            return new OSViewHolder(view);
        }

        @Override
        public PhoneViewHolder onCreateChildViewHolder(ViewGroup parent, final int viewType) {
            LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
            View view = inflater.inflate(R.layout.child_view_holder, parent, false);

            return new PhoneViewHolder(view);
        }

        @Override
        public void onBindChildViewHolder(PhoneViewHolder holder, int flatPosition, ExpandableGroup group, int childIndex) {
            final Phone phone = ((MobileOS) group).getItems().get(childIndex);
            holder.onBind(phone,group);
        }

        @Override
        public void onBindGroupViewHolder(OSViewHolder holder, int flatPosition, ExpandableGroup group) {
            holder.setGroupName(group);
            ImageView mobile_os= view.findViewById(R.id.mobile_os);


            if (flatPosition==1){
                mobile_os.setImageResource(R.mipmap.h);
                Toast.makeText(activity, "1", Toast.LENGTH_SHORT).show();
            }
            if(flatPosition==2) {
                mobile_os.setImageResource(R.mipmap.i);
                Toast.makeText(activity, "2", Toast.LENGTH_SHORT).show();
            }
            if (flatPosition==3){
                mobile_os.setImageResource(R.mipmap.d);
                Toast.makeText(activity, "3", Toast.LENGTH_SHORT).show();
            }
            if (flatPosition==4){
                mobile_os.setImageResource(R.mipmap.maxresdefaultd);
            }


        }
}