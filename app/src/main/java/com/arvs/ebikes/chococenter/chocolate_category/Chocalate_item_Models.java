package com.arvs.ebikes.chococenter.chocolate_category;

public class Chocalate_item_Models {

    private int image_choco_offer;
    private int image_choco_second;

    public Chocalate_item_Models() {
    }

    public Chocalate_item_Models(int image_choco_offer, int image_choco_second) {
        this.image_choco_offer = image_choco_offer;
        this.image_choco_second = image_choco_second;
    }

    public int getImage_choco_offer() {
        return image_choco_offer;
    }

    public void setImage_choco_offer(int image_choco_offer) {
        this.image_choco_offer = image_choco_offer;
    }

    public int getImage_choco_second() {
        return image_choco_second;
    }

    public void setImage_choco_second(int image_choco_second) {
        this.image_choco_second = image_choco_second;
    }
}
